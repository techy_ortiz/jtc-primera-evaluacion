package datos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import utils.JdbcUtil;
import utils.LogUtil;

/**
 * Clase LogConversionesManager. 
 * Implementa las operaciones sobre la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class LogConversionesManager {
    
    /**
     * Retorna la cantidad de registros en la tabla o -1 en caso de error
     * @return Nro de registros o -1 en caso de error
     */
    public int getCantidadRegistros() {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            int nro = -1;
            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select count(*) from LOG_CONVERSIONES");
                if (rs != null) {
                    if (rs.next()) {
                        nro = rs.getInt(1);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
                        
            return nro;
        } else {
            return -1;
        }        
    }

    /**
     * Metodo que retorna el listado completo de registros de la tabla en BD
     * @return Lista de LogConversiones
     */
    public List<LogConversiones> getAll() {
        List<LogConversiones> resp = new ArrayList<LogConversiones>();
        String ipCliente = null;
        Integer msgRequest =0;
        String msgResponse = null;
        String nombreThread = null;
        Date fecHora = null;
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select * from LOG_CONVERSIONES");
                if (rs != null) {
                    while (rs.next()) {
                        LogConversiones lc = new LogConversiones(ipCliente,msgRequest, msgResponse, nombreThread, fecHora);
                        /*
                         * DONE: Pendiente de realizar el mapeo Objeto Relacional
                         * Ejemplo
                         *   lc.setCampoXX(rs.getCampoXX(1));
                         */
                        lc.setIpCliente(rs.getString("ip_cliente"));
                        lc.setMsgRequest(rs.getInt("msg_request"));
                        lc.setMsgResponse(rs.getString("msg_response"));
                        lc.setNombreThread(rs.getString("nombre_thread"));
                        lc.setFechaHora(rs.getDate("fecha_hora"));
                        resp.add(lc);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
        
        return resp;
    }
    
    /**
     * Busca un registro por su ID
     * @param id ID del registro
     * @return Registro con ID o NULL si no existe
     */
    public LogConversiones buscarPorID(int id) {
    
        LogConversiones lc = null;
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            try {
                PreparedStatement stm = con.prepareStatement("select * from log_conversiones where id_mensaje = ?");
                stm.setInt(1, id);
                ResultSet rs = stm.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                        lc = new LogConversiones();
                        /*
                         * DONE: Pendiente de realizar el mapeo Objeto Relacional
                         * Ejemplo
                         *   lc.setCampoXX(rs.getCampoXX(1));
                         */
                        lc.setIpCliente(rs.getString("ip_cliente"));
                        lc.setMsgRequest(rs.getInt("msg_request"));
                        lc.setMsgResponse(rs.getString("msg_response"));
                        lc.setNombreThread(rs.getString("nombre_thread"));
                        lc.setFechaHora(rs.getDate("fecha_hora"));
                    }
                }
                con.close();
            } catch (Exception ex) {
                LogUtil.ERROR("Error al ejecutar consulta", ex);
            }
                        
            return lc;
        } else {
            return null;
        }
    }
    
    /**
     * Inserta un nuevo registro en tabla 
     * @param LogConversiones
     */
    public void insertarNuevoRegistro(LogConversiones nuevoRegistro) {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
            	/*
            	 * DONE: Crear un preparedStatement para el insert y leer los parametros de cada campo
            	 * con las funciones get del objeto nuevoRegistro
            	 */
                PreparedStatement stm = con.prepareStatement("insert into log_conversiones(NOMBRE_THREAD,IP_CLIENTE,FECHA_HORA,MSG_REQUEST,MSG_RESPONSE)  values (?,?,?,?,?)");
                System.out.println(nuevoRegistro.getNombreThread());
                stm.setString(1, nuevoRegistro.getNombreThread());
                stm.setString(2, nuevoRegistro.getIpCliente());
                
                Timestamp fhTimeStamp = new Timestamp(nuevoRegistro.getFechaHora().getTime());
                stm.setTimestamp(3, fhTimeStamp);
               // stm.setDate(3, (Date) nuevoRegistro.getFechaHora());
                stm.setInt(4, nuevoRegistro.getMsgRequest());
                stm.setString(5, nuevoRegistro.getMsgResponse());
                
                int rs = stm.executeUpdate();
                if (rs == 1) {
                	//System.out.println("Registro creado exitosamente: " + nuevoRegistro);
                    LogUtil.INFO("Registro creado exitosamente: " + nuevoRegistro);
                }
                con.commit();
                con.close();
            } catch (Exception e) {
            	//System.out.println("Error al insertar registro: " + e.getMessage());
                LogUtil.ERROR("Error al ejecutar insertar ", e);
            }
        }
    }

    /**
     * Elimina el registro con ID recibido
     * @param codigo ID del registro a eliminar
     */
    public void eliminarRegistro(int id) {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                PreparedStatement stm = con.prepareStatement("delete from log_conversiones where id_mensaje = ?");
                stm.setInt(1, id);
                
                int rs = stm.executeUpdate();
                if (rs == 1) {
                    LogUtil.INFO("Registro eliminado exitosamente");
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
    }
    
} //Fin de clase Log Conversiones
