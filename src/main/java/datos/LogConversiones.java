package datos;

import java.util.Date;

/**
 * Clase LogConversiones. Representa a 1 registro de la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class LogConversiones {
    
    //DONE: Crear estructura de la clase, constructor, getters y setters

	private Date fechaHora;
	private Integer idMensaje;
	private String ipCliente;
	private Integer msgRequest;
	private String msgResponse;
	private String nombreThread;
		
	
	/**
	 * @param fechaHora
	 * @param idMensaje
	 * @param ipCliente
	 * @param msgRequest
	 * @param msgResponse
	 * @param nombreThread
	 */
	public LogConversiones(String ipCliente, Integer msgRequest, String msgResponse, String nombreThread, Date fecHora) {
		
		this.setMsgRequest(msgRequest);
		this.setMsgResponse(msgResponse);
		this.setIpCliente(ipCliente);
		this.setNombreThread(nombreThread);
		this.setFechaHora(fecHora);		
	}
	
	
	public LogConversiones() {
	}
	
	public Date getFechaHora() {
		return fechaHora;
	}
	
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	
	public Integer getIdMensaje() {
		return idMensaje;
	}
	
	public void setIdMensaje(Integer idMensaje) {
		this.idMensaje = idMensaje;
	}
	
	public String getIpCliente() {
		return ipCliente;
	}
	
	public void setIpCliente(String ipCliente) {
		this.ipCliente = ipCliente;
	}
	
	public Integer getMsgRequest() {
		return msgRequest;
	}
	
	public void setMsgRequest(Integer msgRequest) {
		this.msgRequest = msgRequest;
	}
	
	public String getMsgResponse() {
		return msgResponse;
	}
	
	public void setMsgResponse(String msgResponse) {
		this.msgResponse = msgResponse;
	}
	
	public String getNombreThread() {
		return nombreThread;
	}
	
	public void setNombreThread(String nombreThread) {
		this.nombreThread = nombreThread;
	}
	
	
	@Override
	public String toString() {
		// DONE Auto-generated method stub
		String registro = getNombreThread() + " - " + getFechaHora()+ " - " + getIpCliente()+ " - " + getMsgRequest() + " - " + getMsgResponse();
    	return registro;
		
	}
	
	
	
} //Fin de clase
