package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;

import datos.LogConversiones;
import datos.LogConversionesManager;
import romano.ConversorRomano;
import romano.NroInvalidoException;
import utils.LogUtil;

public class ConversorRomanoThread extends Thread {
    
	//Referencia al cliente que sera atendido
    private Socket cliente;
    private String nroRomano = null;
    private String msgError = null;
    private int numeroDecimal = 0;
    private LogConversionesManager logConversionesMng;
    LogConversiones nuevoLog;
    private String nomHilo = null;;
    private String ipCliente;
    
    public ConversorRomanoThread(Socket cliente) {
        this.cliente = cliente;
    }
    
    @Override
    public void run() {
    	LogUtil.INFO("Nueva peticion recibida...");
        //DONE: Pendiente de implementacion de logica que parsea peticion, procesa request y almacena resultado en la bd
    	/* Socket cliente = new Socket("direccionIP", puerto); */
        //Creamos un cliente socket en Localhost, puerto 6400
    try { 
    	
     	 //cuando llegue a esta linea, es porque la anterior se ejecuto y avanzo
        //lo cual quiere decir que algun cliente se conecto a este servidor
        //obtenemos el OutputStream del cliente, para escribirle algo
        OutputStream clientOut = cliente.getOutputStream();
        PrintWriter pw = new PrintWriter(clientOut, true);
        
        //obtenemos el InputStream del cliente, para leer lo que nos dice
        InputStream clientIn = cliente.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
       
        //Leemos la primera linea del mensaje recibido
        String mensajeRecibido = br.readLine();
        //System.out.println("msg recibido " + mensajeRecibido);
        
        if (mensajeRecibido != null){
        	if (mensajeRecibido.trim().equalsIgnoreCase("fin")){
        		// asumimos que finaliza correctamente
        		System.exit(0);
        	} if (mensajeRecibido.trim().startsWith("conv")){
        		 try {
        			 
        			   //extraemos el numero ingresado del mensaje
        			    String strnum = mensajeRecibido.substring(4);

        	        	// parseamos a integer el string recibido
        	        	numeroDecimal = Integer.parseInt(strnum.trim());
        	        
        	        	// invocamos al metodo conversor a numero romano
        				nroRomano = ConversorRomano.decimal_a_romano(numeroDecimal);
        			
        			} catch (NroInvalidoException e) {
        				// DONE Auto-generated catch block
        				msgError = e.getMessage();
        				LogUtil.ERROR(msgError);
        				pw.println(msgError);
        			}
        	}
        } else {
        	msgError = "Mensaje recibido no valido.. reintente por favor!!";
        	pw.println(msgError);
        }
        
        // obtenemos el nombre del thread
        nomHilo = this.getName();
        
        // obtenemos la ip del server
        ipCliente = cliente.getInetAddress().getHostAddress();
        
      
    	LogUtil.INFO("Finalizando atencion de la peticion recibida...");
    	
    	
    	//------------------------------------------------------------------------------------
    	
    	LogUtil.INFO("Inicio de registro de la peticion en la tabla log Conversiones..." + nomHilo);
    	
    	//instanciamos la clase manager de LogConversiones
    	logConversionesMng = new LogConversionesManager();
    	
    	
    	//seteo el numero romano o el mensaje a una variable para registrar en la tabla
    	String msgResponse = null;
    	if (nroRomano != null) {
    		msgResponse = nroRomano;
       	} else {
       		msgResponse = msgError;
       	}
    	
    	//instancio el objeto LogConversiones, utilizando el constructor redefinido de la clase
    	nuevoLog = new LogConversiones(ipCliente, numeroDecimal, msgResponse, nomHilo, ConversorRomanoMultihilo.fechora);
    	
    	//System.out.println(nuevoLog.toString());
    	//nuevoLog.setNombreThread(nomHilo);
    	//nuevoLog.setFechaHora(ConversorRomanoMultihilo.fechora);
    	//nuevoLog.setIpCliente(ipClienteStr); 
    	//nuevoLog.setMsgRequest(numeroDecimal);
    		
    	//if (nroRomano != null) {
    	// nuevoLog.setMsgResponse(nroRomano);
    	//} else {
        // nuevoLog.setMsgResponse(msgError);
    	//}
    	
    	// inserto el nuevo registro
    	logConversionesMng.insertarNuevoRegistro(nuevoLog);
    	
    	//logueo finalizacion de la insercion en la tabla
    	LogUtil.INFO("Fin de registro en la tabla log Conversiones..." + nomHilo );
    	
    	//Enviamos la respuesta al cliente
        pw.println(nroRomano); 
    	
    	//cerramos conexion con el cliente luego de cerrar los flujos
        clientIn.close();
        clientOut.close();
        cliente.close();
    } catch (ConnectException ce) {
    	System.out.println(ce.getMessage());
    	LogUtil.INFO("No puede conectarse al servidor. " + ce.getMessage());
        
    } catch (IOException ie) {
        LogUtil.INFO("I/O Error. " + ie.getMessage());
     } finally{
    	//cerramos conexion con el cliente luego de cerrar los flujos
         try {
			cliente.close();
		} catch (IOException e) {
			
		}
     }
    }
    
}
