package socket;

import java.net.*;
import java.util.Date;
import java.io.*;
import utils.LogUtil;

/**
 * Clase ConversorRomanoServer Multihilo
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ConversorRomanoMultihilo {
	
	static Date fechora = new Date();
	
    public static void main(String[] args) throws IOException {
        ServerSocket servidor = null;
        Socket cliente;
        ConversorRomanoThread crt;
        
      
        try {
            //Abre un servidor socket local en el puerto 6400
        	LogUtil.INFO("Iniciando socket server en puerto 6400");
            servidor = new ServerSocket(6400);
            
        } catch (IOException ie) {
            System.err.println("Error al abrir socket. " + ie.getMessage());
            System.exit(1);
        }
        
         LogUtil.INFO("Socket iniciado, se atienden peticiones..");
        //DONE: Pendiente de implementacion del procesador de peticiones
         
         while (true) {
             try {
                 //el servidor socket queda esperando indefinidamente conexiones
                 //cuando algun cliente se conecte, en el objeto "cliente" tendremos
                 //una referencia al cliente conectado
                 cliente = servidor.accept();
                 
                 // instanciamos la clase thread
                 crt = new ConversorRomanoThread(cliente);
                 
                 // seteo un nombre al thread
                 crt.setName("New Conexion at: " + fechora);
                 // inicio
                 crt.start();
                 
                
             } catch (IOException ie) {
            	 System.out.println("Error al procesar socket " + ie.getMessage());
            	 LogUtil.ERROR("Error al procesar socket. " + ie.getMessage());
            	 //servidor.close();
            	 
             } 
         }
         
         
    }

}